##Phage Roles##
1. Landing Page
*It will ask permision for users geolocation
- If permission is given it will pull out the nearest city according to users geolocation
- If not it will direct the users to a default city based on users IP adress ( If the country is not listed nearest country will be shown)
* Landing page will be showing newest places.
* Landing page will also show blog posts I have not placed in the example but I will send you a much up about this.
* We can add a feature places as well for the next version

2. Sign Up Page
- We will allow users to sign up with their twitter accounts or gmails or plain emails

3. Sign In Page
- It will have forgot password section
-Remember me section

4. Blog Post Page
* It will show list of the blogs entered

5.Place Page will have 
-About Place: 
-Map
-Adress:
-Postal Code:
-City:
-Country:
-Phone:
-Reviews 
  * Users will be able to rate, report the reviews
  * Users will be able to upload pictures for their reviews.

6. Add Place Page will have:
About:
Upload picture
Category:
Adress:
Postal Code:
Sellect City
Country:
Phone:

7. Add City Page:

 According to add a city we will give a form for a user to introduce their self also add a place and ask the user if they want to be a contributer.

For will contain:
Name:
Last Name:
Email:
Why you would like to add your city?

Next button will take the user to add place.

After user add places admin will check both city and place. Also users application. Users application will be restored in a different category also city places will hold up as well until city is approved.

8. Subscription Page (Upgrade Business)



##User Functions##
* User can assign their cities
* Users can review places
* Users can send messages each other
* Users can rate users reviews

(All of these options are given on example coding on Yelp Clone App)

https://github.com/programmertoni/LiveCoding-Yelp-clone-app


##User Roles##
-Admin
-City Major (it will be a title the first user to contribute most for the city will be selected by users every year)
-Place Owner
- Plain User

##Subscription Options##
Subscribe for feature the page

Package 1
Feature the page for 3 months
Package 2
Feature the page for 6 months
Profesional Review

Package 3
Feature the page for 12 months
Profesional Review

Prices will be added from admin section

## Admin Section ##

- User pages ( we will be able to see users, delete, ban)
- Place Pages ( Approve, add, delete places)
- Category Pages ( Able to add place categories)
- City Pages ( See the current cities with their current number of places)
- Mayor Applicants ( See the city applications and approve or deny)
- Blog page
- Send Email (Email cue for entire users same as set phrase only the difference we will update everything by city)
- Subscription price place